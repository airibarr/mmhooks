FROM python:3.11-slim

RUN apt-get update && apt-get install -y \
    libcairo2 \
    && rm -rf /var/lib/apt/lists/*

COPY ./requirements.txt /requirements.txt
RUN pip install --no-cache-dir -r /requirements.txt

COPY ./start.sh /start.sh
RUN chmod +x /start.sh

ENV PORT 8000
EXPOSE 8000/tcp
ENV PYTHONPATH=/app
ENV HOME=/tmp

COPY ./app /app

CMD ["/start.sh"]
