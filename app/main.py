from typing import Annotated
from pydantic.color import Color
from pydantic import HttpUrl, AnyUrl
from enum import Enum
from fastapi import FastAPI, status, Request, Query, Path, HTTPException
from fastapi.responses import JSONResponse, FileResponse, Response
from fastapi.staticfiles import StaticFiles
import os.path
import logging
import random
import re
import requests
from pprint import pformat
from pybadges import badge
from cairosvg import svg2png
import html
from datetime import datetime, timezone, timedelta
import pytz

app = FastAPI()

IMGDIR = 'https://mmhooks.web.cern.ch/img/{}'
WEBTOOLS_ROTA = 'https://op-webtools.web.cern.ch/planning-dashboard/backend/api/v1/roles/{}/onduty'

# For health checks
@app.get('/healthz', summary='API health check')
def ping():
    return

logger = logging.getLogger('uvicorn.error')
logging.getLogger('uvicorn.access').addFilter(lambda record: 'GET /healthz' not in record.getMessage())

@app.get('/echo')
def echo(request: Request):
    return {
        'response_type': 'in_channel',
        'text': "Here's what I got:",
        'attachments': [{
            'fallback': 'Logs',
            'author_name': request.query_params.get('user_name'),
            'text':
                f'Headers:\n {pformat(request.headers)}\n\n'
                f'Query Parameters:\n {pformat(request.query_params)}\n\n'
                f'Path Parameters:\n {pformat(request.path_params)}\n\n'
        }],
        'icon_url': IMGDIR.format('bot.png'),
        'username': 'Echobot'
    }

class StaticFilesCache(StaticFiles):
    def __init__(self, *args, cachecontrol="public, max-age=86400, must-revalidate, immutable", **kwargs):
        self.cachecontrol = cachecontrol
        super().__init__(*args, **kwargs)

    def file_response(self, *args, **kwargs) -> Response:
        resp: Response = super().file_response(*args, **kwargs)
        resp.headers.setdefault("Cache-Control", self.cachecontrol)
        return resp

app.mount(path="/img", app=StaticFilesCache(directory='app/img'), name="img")

@app.get('/snow')
def snow(request: Request):
    response = {
        'response_type': 'in_channel',
        'icon_url': IMGDIR.format('snowman.png'),
        'username': 'Frosty'
    }

    if random.randrange(2):
        response['text'] = 'Have you considered creating a [SNOW ticket](https://cern.service-now.com/service-portal/)?'
    else:
        response['text'] = '![Have you considered creating a SNOW ticket?](%s)' % IMGDIR.format('chris-ticket.png')

    logger.info('snow: %s', response)
    return response

@app.get('/onlybots')
def onlybots():
    return {
        'response_type': 'in_channel',
        'text': 'Hi human slave! This channel is only for us bots. Please shut up!',
        'icon_url': IMGDIR.format('hedionism-bot.png'),
        'username': 'Botty'
    }

@app.get('/itssb')
def itssb(text: str = None):
    response = {
        'response_type': 'in_channel',
        'text': 'Have you checked the [ITSSB](https://cern.ch/itssb)?',
        'icon_url': IMGDIR.format('keep-calm-and-itssb.png'),
        'username': 'ITSSB'
    }

    if text:
        otg = re.search('OTG\d+', text, flags=re.IGNORECASE)
        if otg:
            otg = otg.group(0).upper()
            response['text'] += f' [{otg}](https://cern.service-now.com/service-portal?id=outage&n={otg}) looks like it could be relevant.'
        else:
            response['text'] += f' {text}'

    return response

@app.get('/regenrepo')
def itssb():
    return {
        'response_type': 'in_channel',
        'text': 'Have you tried running `koji regen-repo`?',
        'icon_url': IMGDIR.format('bot.png'),
        'username': 'Regeneron'
    }


def get_rotaer(rota_id):
    try:
        rota = requests.get(WEBTOOLS_ROTA.format(rota_id))
        assert rota.status_code == 200

        data = rota.json()
        assert len(data) > 0, "Rota is empty"

        # Return the list of rotaers and the earliest end date of their rotas
        # The API returns dates in CERN time, but with no timezone information
        return (
            [x['user_id'] for x in data],
            min([
                pytz.timezone('Europe/Zurich').localize(datetime.fromisoformat(data[0]['date_end']))
                for x in data
            ])
        )
    except Exception as e:
        logger.error(f'Error retrieving rota {rota_id}: {e}')
        return [None, None]

@app.get('/linuxrota')
def linuxrota():
    rotaer, expiration = get_rotaer(6384)

    if rotaer:
        return {
            'response_type': 'in_channel',
            'text': f'@{rotaer[0]} is on Penguin Patrol today!',
            'icon_url': IMGDIR.format('shiftie.png'),
            'username': 'Shiftie'
        }

    return {
        'response_type': 'in_channel',
        'text': "I could not retrieve the Linux Rota. I know, I'm as disappointed as you are!",
        'icon_url': IMGDIR.format('shiftie.png'),
        'username': 'Shiftie'
    }

class ColorNames(str, Enum):
    critical = 'critical'
    important = 'important'
    success = 'success'
    informational = 'informational'
    inactive = 'inactive'

    def as_hex(self):
        colors: dict[str, str] = {
            'critical': '#e05d44',
            'important': '#fe7d37',
            'success': '#4c1',
            'informational': '#007ec6',
            'inactive': '#9f9f9f'
        }
        return colors[self.value]

@app.get('/badge', summary='Create a badge with the given text')
def badge_text(
    left_text:   Annotated[str, Query(description="Text on the left-hand side of the badge")],
    right_text:  Annotated[str, Query(description="Text on the right-hand side of the badge")],
    left_color:  Annotated[Color | ColorNames, Query(description="Color of the left-hand side of the badge")] = Color('#555'),
    right_color: Annotated[Color | ColorNames, Query(description="Color of the right-hand side of the badge")] = Color('#007ec6'),
    logo:        Annotated[HttpUrl, Query(description="URL of the logo to display on the left-hand side of the badge")] = None,
    link:        Annotated[AnyUrl, Query(description="URL to link to when the badge is clicked (SVG only)")] = None,
    png:         Annotated[bool, Query(description="Generate a PNG (instead of an SVG)")] = False,
    expires:     Annotated[datetime, Query(description="Expiration time for caches (ISO 8601 with timezone)")] = None,
):
    """
    Create a badge with the given left and right text.

    Colors can be specified as [CSS3 values](https://www.w3.org/TR/css-color-3/#svg-color) (e.g. `#ff0000`, `red`, etc.)
    or by certain predefined names (`critical`, `important`, `success`, `informational`, `inactive`).
    """

    if png and link:
        raise HTTPException(422, "PNG badges cannot have links")

    headers = { 'cache-control': 'public' }
    if expires:
        headers['expires'] = expires.astimezone(timezone.utc).strftime('%a, %d %b %Y %H:%M:%S GMT')

    try:
        svg_badge = badge(
            left_text=html.escape(left_text),
            right_text=html.escape(right_text),
            left_color=left_color.as_hex(),
            right_color=right_color.as_hex(),
            logo=str(logo) if logo else None,
            embed_logo=True,
            whole_link=str(link) if link else None,
        )
    except ValueError as e:
        if 'expected an image' in str(e):
            raise HTTPException(
                422, [{ "detail": [{
                        "type": "url_parsing",
                        "loc": ["query", "logo"],
                        "msg": str(e),
                        "input": str(logo),
                        "ctx": { "error": "URL must be a valid image" }
                }] }]
            )
        else:
            raise HTTPException(422, str(e))
    except requests.exceptions.RequestException as e:
        # This will be because of the logo as well
        raise HTTPException(
                422, [{ "detail": [{
                        "type": "url_parsing",
                        "loc": ["query", "logo"],
                        "msg": str(e),
                        "input": str(logo),
                        "ctx": { "error": "URL must be available" }
                }] }]
            )
    except Exception as e:
        raise HTTPException(422, str(e))

    if png:
        return Response(content=svg2png(bytestring=svg_badge), media_type="image/png", headers=headers)

    return Response(content=svg_badge, media_type="image/svg+xml", headers=headers)

@app.get('/badge/rota/{rota_id}', summary='Create a badge with the person(s) on call for a given OP Webtools Planning rota/role ID')
def badge_rota(
    rota_id:     Annotated[int,  Path(description="OP Webtools Planning rota/role ID")],
    title:       Annotated[str,  Query(description="Text on the left-hand side of the badge")] = 'On Duty',
    left_color:  Annotated[Color | ColorNames, Query(description="Color of the left-hand side of the badge")] = Color('#555'),
    right_color: Annotated[Color | ColorNames, Query(description="Color of the right-hand side of the badge")] = Color('#007ec6'),
    logo:        Annotated[HttpUrl, Query(description="URL of the logo to display on the left-hand side of the badge")] = None,
    link:        Annotated[AnyUrl, Query(description="URL to link to when the badge is clicked (SVG only)")] = None,
    png:         Annotated[bool, Query(description="Generate a PNG (instead of an SVG)")] = False,
    noat:        Annotated[bool, Query(description="Don't prefix usernames with @")] = False,
    max_ttl:     Annotated[int, Query(
        description="Maximum TTL for cache in minutes. Will be shorter if the rota ends earlier.",
        ge=0,
        le=20160, # 2 weeks
    )] = 60,
):
    """
    Create a badge with the person(s) on call for a given OP Webtools Planning rota/role ID. You can
    find your rota/role ID here:

    ![Rota/Role ID](/img/docs/roleid.png)

    """
    rotaer, expiration = get_rotaer(rota_id)

    if not rotaer:
        rotaer = ['Unknown']
    elif noat == False:
        rotaer = [f'@{x}' for x in rotaer]

    max_expiration = datetime.now(timezone.utc) + timedelta(minutes=max_ttl)
    if expiration:
        expiration = min(expiration, max_expiration)
    else:
        expiration = max_expiration

    return badge_text(
        left_text=title,
        right_text=', '.join(rotaer),
        left_color=left_color,
        right_color=right_color,
        logo=logo,
        link=link,
        png=png,
        expires=expiration,
    )
